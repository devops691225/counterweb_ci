package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}
       @Test
    public void testScreenshot() throws Exception {
        driver.get(testURL);
        captureScreenshot(driver, screenshotLocation);
    }
 
 
    public void captureScreenshot(WebDriver driver, String filename) throws IOException
    {
       
       
        File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(filename));
    }  
}
